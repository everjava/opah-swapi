package com.example.opahswapi.core.people.usecase;

import com.example.opahswapi.core.people.entity.People;
import com.example.opahswapi.core.shared.entity.Results;
import com.example.opahswapi.core.people.ports.PeopleService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class PeopleUseCaseTest {

    private PeopleFindAllUseCase peopleFindAllUseCase;
    private PeopleFindByIdUseCase peopleFindByIdUseCase;

    @Mock
    private PeopleService peopleService;

    @BeforeEach
    public void setUp(){
        peopleFindAllUseCase = new PeopleFindAllUseCase(peopleService);
        peopleFindByIdUseCase = new PeopleFindByIdUseCase(peopleService);
    }

    @Test
    public void testFindById_ShouldNotBeEmpty(){
        //Given
        People p1 = new People();
        p1.setName("Maria");
        List<People> peopleList = List.of(p1);
        Mockito.when(peopleService.findById(1)).thenReturn(p1);

        //When
        People resultsUseCase = peopleFindByIdUseCase.execute(1);

        //Then
        Assertions.assertNotNull(resultsUseCase);
    }

    @Test
    public void testFindAll_ShouldNotBeEmpty(){
        //Given
        Results<People> results = new Results<>();
        People p1 = new People();
        p1.setName("Maria");
        List<People> peopleList = List.of(p1);
        results.setResults(peopleList);
        Mockito.when(peopleService.findAll()).thenReturn(results);

        //When
        Results<People> resultsUseCase = peopleFindAllUseCase.execute();

        //Then
        Assertions.assertFalse(resultsUseCase.getResults().isEmpty());
    }

}
