package com.example.opahswapi.core.films.usecase;


import com.example.opahswapi.core.films.entity.Film;
import com.example.opahswapi.core.films.ports.FilmService;
import com.example.opahswapi.core.shared.entity.Results;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class FilmUseCaseTest {

    private FilmFindAllUseCase filmFindAllUseCase;
    private FilmFindByIdUseCase filmFindByIdUseCase;

    @Mock
    private FilmService filmService;

    @BeforeEach
    public void setUp(){
        filmFindAllUseCase = new FilmFindAllUseCase(filmService);
        filmFindByIdUseCase = new FilmFindByIdUseCase(filmService);
    }


    @Test
    void testFindAll_ShouldNotBeEmpty(){
        Film film = new Film();
        film.setTitle("Matrix");
        List<Film> films = List.of(film);
        Results<Film> results = new Results<>();
        results.setResults(films);
        Mockito.when(filmService.findAll()).thenReturn(results);

        Results<Film> filmResults = filmFindAllUseCase.execute();

        Assertions.assertFalse(filmResults.getResults().isEmpty());
    }

    @Test
    void testFindById_ShouldNotBeEmpty(){
        Film film = new Film();
        film.setTitle("Matrix");
        Mockito.when(filmService.findById(1)).thenReturn(film);

        Film filmResults = filmFindByIdUseCase.execute(1);

        Assertions.assertNotNull(filmResults);
    }

}
