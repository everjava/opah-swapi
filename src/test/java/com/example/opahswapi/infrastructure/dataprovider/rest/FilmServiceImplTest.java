package com.example.opahswapi.infrastructure.dataprovider.rest;

import com.example.opahswapi.core.films.entity.Film;
import com.example.opahswapi.core.shared.entity.Results;
import com.example.opahswapi.infrastructure.dataprovider.rest.impl.FilmServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class FilmServiceImplTest {

    @Mock
    private SwapiClient swapiClient;
    @InjectMocks
    private FilmServiceImpl filmService;

    @Test
    void testFilmFindById_ShouldReturnTitleMatrix(){
        Film film = new Film();
        film.setTitle("Matrix");
        ResponseEntity<Film> responseEntity = ResponseEntity.ok(film);
        Mockito.when(swapiClient.filmById(1)).thenReturn(responseEntity);

        Film response = filmService.findById(1);

        Assertions.assertEquals("Matrix" , response.getTitle());
    }

    @Test
    void testFilmFindAll_ShouldReturnListNotEmpty(){
        Film film = new Film();
        film.setTitle("Matrix");
        List<Film> films = List.of(film);
        Results results = new Results();
        results.setResults(films);
        ResponseEntity<Results<Film>> responseEntity = ResponseEntity.ok(results);
        Mockito.when(swapiClient.filmList()).thenReturn(responseEntity);

        Results response = filmService.findAll();

        Assertions.assertFalse(response.getResults().isEmpty());
    }

}
