package com.example.opahswapi.infrastructure.dataprovider.rest;

import com.example.opahswapi.core.people.entity.People;
import com.example.opahswapi.core.shared.entity.Results;
import com.example.opahswapi.infrastructure.dataprovider.rest.impl.PeopleServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class PeopleServiceImplTest {

    @Mock
    private SwapiClient swapiClient;
    @InjectMocks
    private PeopleServiceImpl peopleService;

    @Test
    void testPeopleFindById_ShouldReturnTitleMatrix() {
        People people = new People();
        people.setName("Luke");
        ResponseEntity<People> responseEntity = ResponseEntity.ok(people);
        Mockito.when(swapiClient.peopleById(1)).thenReturn(responseEntity);

        People response = peopleService.findById(1);

        Assertions.assertEquals("Luke", response.getName());
    }

    @Test
    void testPeopleFindAll_ShouldReturnListNotEmpty() {
        People people = new People();
        List<People> films = List.of(people);
        Results results = new Results();
        results.setResults(films);
        ResponseEntity<Results<People>> responseEntity = ResponseEntity.ok(results);
        Mockito.when(swapiClient.peopleList()).thenReturn(responseEntity);

        Results response = peopleService.findAll();

        Assertions.assertFalse(response.getResults().isEmpty());
    }

}
