package com.example.opahswapi.infrastructure.delivery.film;

import com.example.opahswapi.core.films.entity.Film;
import com.example.opahswapi.core.films.usecase.FilmFindAll;
import com.example.opahswapi.core.films.usecase.FilmFindById;
import com.example.opahswapi.core.shared.entity.Results;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;


//@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = FilmController.class)
public class FilmControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private FilmFindAll filmFindAll;
    @MockBean
    private FilmFindById filmFindById;

    @Test
    void testFindById_ShouldReturneSameContentEquals() throws Exception {
        Film film = new Film();
        film.setTitle("Matrix");
        Mockito.when(filmFindById.execute(1)).thenReturn(film);
        String expected = mapper.writeValueAsString(film);

        MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/film/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.  content().string(expected)).andReturn();

        String content = response.getResponse().getContentAsString();
        Assertions.assertEquals(expected, content);
    }


    @Test
    void testFindAll_ShouldReturneSameContentEquals() throws Exception {
        Film film = new Film();
        film.setTitle("Matrix");
        Results<Film> results = new Results<>();
        results.setResults(List.of(film));
        Mockito.when(filmFindAll.execute()).thenReturn(results);


        MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/film"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String content = response.getResponse().getContentAsString();
        String expected = mapper.writeValueAsString(results);
        assertEquals(expected, content);
    }

}
