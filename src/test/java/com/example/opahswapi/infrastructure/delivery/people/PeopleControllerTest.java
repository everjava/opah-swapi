package com.example.opahswapi.infrastructure.delivery.people;

import com.example.opahswapi.core.films.entity.Film;
import com.example.opahswapi.core.people.entity.People;
import com.example.opahswapi.core.people.usecase.PeopleFindAll;
import com.example.opahswapi.core.people.usecase.PeopleFindById;
import com.example.opahswapi.core.shared.entity.Results;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@WebMvcTest( controllers = PeopleController.class)
public class PeopleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private PeopleFindAll peopleFindAll;

    @MockBean
    private PeopleFindById peopleFindById;


    @Test
    void testFindById_ShouldReturneSameContentEquals() throws Exception {
        People ppl = new People();
         ppl.setName("Luke");
        Mockito.when(peopleFindById.execute(1)).thenReturn(ppl);
        String expected = mapper.writeValueAsString(ppl);

        MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/people/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.  content().string(expected)).andReturn();

        String content = response.getResponse().getContentAsString();
        Assertions.assertEquals(expected, content);
    }

    @Test
    void testFindAll_ShouldReturneSameContentEquals() throws Exception {
        People ppl = new People();
        ppl.setName("Luke");
        Results<People> results = new Results<>();
        results.setResults(List.of(ppl));
        Mockito.when(peopleFindAll.execute()).thenReturn(results);

        MvcResult response = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/people"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String content = response.getResponse().getContentAsString();
        String expected = mapper.writeValueAsString(results);
        assertEquals(expected, content);
    }

}
