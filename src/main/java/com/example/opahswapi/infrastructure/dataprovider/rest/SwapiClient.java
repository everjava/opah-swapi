package com.example.opahswapi.infrastructure.dataprovider.rest;


import com.example.opahswapi.core.films.entity.Film;
import com.example.opahswapi.core.people.entity.People;
import com.example.opahswapi.core.shared.entity.Results;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;



@FeignClient(name = "swapi", url = "https://swapi.dev/api/")
public interface SwapiClient {

    @GetMapping("/films")
    ResponseEntity<Results<Film>> filmList();

    @GetMapping("/films/{id}")
    ResponseEntity<Film> filmById(@PathVariable("id") Integer id);

    @GetMapping("/people")
    ResponseEntity<Results<People>> peopleList();

    @GetMapping("/people/{id}")
    ResponseEntity<People> peopleById(@PathVariable("id") Integer id);

}
