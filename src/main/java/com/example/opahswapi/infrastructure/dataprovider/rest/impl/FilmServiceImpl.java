package com.example.opahswapi.infrastructure.dataprovider.rest.impl;

import com.example.opahswapi.core.films.entity.Film;
import com.example.opahswapi.core.films.ports.FilmService;
import com.example.opahswapi.core.shared.entity.Results;
import com.example.opahswapi.infrastructure.dataprovider.rest.SwapiClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class FilmServiceImpl  implements FilmService {

    private SwapiClient swapiClient;

    @Override
    public Film findById(Integer id) {
        log.info("Chamando API /film/"+id);
        return swapiClient.filmById(id).getBody();
    }

    @Override
    public Results<Film> findAll() {
        log.info("Chamando API /film");
        return swapiClient.filmList().getBody();
    }
}
