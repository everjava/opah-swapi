package com.example.opahswapi.infrastructure.dataprovider.rest.impl;

import com.example.opahswapi.core.people.entity.People;
import com.example.opahswapi.core.shared.entity.Results;
import com.example.opahswapi.core.people.ports.PeopleService;
import com.example.opahswapi.infrastructure.dataprovider.rest.SwapiClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class PeopleServiceImpl  implements PeopleService {

    private SwapiClient swapiClient;

    @Override
    public People findById(Integer id) {
        log.info("Chamando API /people/"+id);
        return swapiClient.peopleById(id).getBody();
    }

    @Override
    public Results<People> findAll() {
        log.info("Chamando API /people");
        return swapiClient.peopleList().getBody();
    }
}
