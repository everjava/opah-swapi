package com.example.opahswapi.infrastructure.configuration;

import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class AppExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception e, HttpServletResponse response) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", "Ocorreu um erro inesperado 1");
        body.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<?> handleFeignStatusException(FeignException e, HttpServletResponse response) {
        response.setStatus(e.status());
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", "Ocorreu um erro ao acessar api starwars");
        body.put("status", HttpStatus.valueOf(response.getStatus()));
        return new ResponseEntity<>(body, HttpStatus.valueOf(response.getStatus()));
    }

    @ExceptionHandler(FeignException.NotFound.class)
    public ResponseEntity<?> handleFeignNotFoundException(FeignException.NotFound e, HttpServletResponse response) {
        response.setStatus(e.status());
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", "Recurso não encontrado");
        body.put("status", HttpStatus.valueOf(response.getStatus()));
        return new ResponseEntity<>(body, HttpStatus.valueOf(response.getStatus()));
    }

    @ExceptionHandler(FeignException.BadRequest.class)
    public ResponseEntity<?> handleFeignBadRequestException(FeignException.BadRequest e, HttpServletResponse response) {
        response.setStatus(e.status());
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", "Valor de entrada inválido");
        body.put("status", HttpStatus.valueOf(response.getStatus()));
        return new ResponseEntity<>(body, HttpStatus.valueOf(response.getStatus()));
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<?> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e, HttpServletResponse response) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", "Valor do parametro inválido");
        body.put("status", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }
}
