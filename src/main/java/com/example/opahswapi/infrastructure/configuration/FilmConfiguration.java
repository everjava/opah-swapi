package com.example.opahswapi.infrastructure.configuration;

import com.example.opahswapi.core.films.usecase.FilmFindAllUseCase;
import com.example.opahswapi.core.films.usecase.FilmFindByIdUseCase;
import com.example.opahswapi.infrastructure.dataprovider.rest.SwapiClient;
import com.example.opahswapi.infrastructure.dataprovider.rest.impl.FilmServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilmConfiguration {

    @Autowired
    private SwapiClient swapiClient;

    public FilmServiceImpl filmServiceImpl(){
        return new FilmServiceImpl(swapiClient);
    }

    @Bean
    public FilmFindAllUseCase filmFindAll(){
        return new FilmFindAllUseCase(filmServiceImpl());
    }

    @Bean
    public FilmFindByIdUseCase filmFindById(){
        return new FilmFindByIdUseCase(filmServiceImpl());
    }
}
