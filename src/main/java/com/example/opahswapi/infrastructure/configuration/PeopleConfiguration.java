package com.example.opahswapi.infrastructure.configuration;

import com.example.opahswapi.core.people.usecase.PeopleFindAll;
import com.example.opahswapi.core.people.usecase.PeopleFindAllUseCase;
import com.example.opahswapi.core.people.usecase.PeopleFindByIdUseCase;
import com.example.opahswapi.infrastructure.dataprovider.rest.SwapiClient;
import com.example.opahswapi.infrastructure.dataprovider.rest.impl.PeopleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PeopleConfiguration {

    @Autowired
    private SwapiClient swapiClient;

    public PeopleServiceImpl peopleServiceImpl(){
        return new PeopleServiceImpl(swapiClient);
    }

    @Bean
    public PeopleFindAllUseCase peopleFindAll(){
        return new PeopleFindAllUseCase(peopleServiceImpl());
    }

    @Bean
    public PeopleFindByIdUseCase peopleFindById(){
        return new PeopleFindByIdUseCase(peopleServiceImpl());
    }

}
