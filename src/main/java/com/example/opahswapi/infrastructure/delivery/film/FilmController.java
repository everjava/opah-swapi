package com.example.opahswapi.infrastructure.delivery.film;

import com.example.opahswapi.core.films.entity.Film;
import com.example.opahswapi.core.films.usecase.FilmFindAll;
import com.example.opahswapi.core.films.usecase.FilmFindById;
import com.example.opahswapi.core.shared.entity.Results;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/")
public class FilmController {

    private ObjectMapper mapper;
    private FilmFindAll filmFindAll;
    private FilmFindById filmFindById;

    public FilmController(ObjectMapper mapper, FilmFindAll filmFindAll, FilmFindById filmFindById) {
        this.mapper = mapper;
        this.filmFindAll = filmFindAll;
        this.filmFindById = filmFindById;
    }

    @GetMapping("/film/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Integer id){
        Film film = filmFindById.execute(id);
        return ResponseEntity.ok(film);
    }

    @GetMapping("/film")
    public ResponseEntity<?> findAll(){
        Results<Film> results = filmFindAll.execute();
        return ResponseEntity.ok(results);
    }


}
