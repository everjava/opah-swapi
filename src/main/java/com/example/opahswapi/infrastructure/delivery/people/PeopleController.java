package com.example.opahswapi.infrastructure.delivery.people;


import com.example.opahswapi.core.people.entity.People;
import com.example.opahswapi.core.shared.entity.Results;
import com.example.opahswapi.core.people.usecase.PeopleFindAll;
import com.example.opahswapi.core.people.usecase.PeopleFindById;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/")
public class PeopleController {

    private ObjectMapper mapper;
    private PeopleFindAll peopleFindAll;
    private PeopleFindById peopleFindById;

    public PeopleController(ObjectMapper mapper, PeopleFindAll peopleFindAll, PeopleFindById peopleFindById){
        this.mapper = mapper;
        this.peopleFindAll = peopleFindAll;
        this.peopleFindById = peopleFindById;
    }

    @GetMapping("/people/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Integer id) {
        ModelMapper modelMapper = new ModelMapper();
        People people = peopleFindById.execute(id);
        PeopleDto peopleDto = modelMapper.map(people, PeopleDto.class);
        return ResponseEntity.ok(peopleDto);
    }

    @GetMapping("/people")
    public ResponseEntity<?> findAll() {
        Results<People> results = peopleFindAll.execute();
        List<PeopleDto> peopleDtoList = convertToPeopleDto(results.getResults());
        Results<PeopleDto> resultsPeopleDto = convertToResultPeopleDto(results, peopleDtoList);
        return ResponseEntity.ok(resultsPeopleDto);
    }

    private List<PeopleDto> convertToPeopleDto(List<People> results) {
        ModelMapper modelMapper = new ModelMapper();
        return results
                .stream()
                .map(ppl -> modelMapper.map(ppl, PeopleDto.class))
                .collect(Collectors.toList());
    }

    private Results<PeopleDto> convertToResultPeopleDto(Results<People> results, List<PeopleDto> dtos) {
        ModelMapper modelMapper = new ModelMapper();
        Results<PeopleDto> resultsDto = new Results<PeopleDto>();
        resultsDto = modelMapper.map(results, Results.class);
        resultsDto.setResults(dtos);
        return resultsDto;
    }
}
