package com.example.opahswapi.core.people.usecase;

import com.example.opahswapi.core.people.entity.People;
import com.example.opahswapi.core.shared.entity.Results;


public interface PeopleFindAll {
      Results<People> execute();
}
