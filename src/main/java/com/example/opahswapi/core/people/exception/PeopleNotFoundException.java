package com.example.opahswapi.core.people.exception;

public class PeopleNotFoundException  extends  RuntimeException{

    private static final long serialVersionUID = -6733350281411022605L;

    public PeopleNotFoundException() {
    }

    public PeopleNotFoundException(String message) {
        super(message);
    }

    public PeopleNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PeopleNotFoundException(Throwable cause) {
        super(cause);
    }

    public PeopleNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
