package com.example.opahswapi.core.people.exception;

public class PeopleException  extends  RuntimeException{

    private static final long serialVersionUID = 6334364508933878215L;

    public PeopleException() {
    }

    public PeopleException(String message) {
        super(message);
    }

    public PeopleException(String message, Throwable cause) {
        super(message, cause);
    }

    public PeopleException(Throwable cause) {
        super(cause);
    }

    public PeopleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
