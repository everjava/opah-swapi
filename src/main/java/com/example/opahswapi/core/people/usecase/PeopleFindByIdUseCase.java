package com.example.opahswapi.core.people.usecase;

import com.example.opahswapi.core.people.entity.People;
import com.example.opahswapi.core.people.ports.PeopleService;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PeopleFindByIdUseCase implements PeopleFindById {

    private PeopleService peopleService;

    public People execute(Integer id) {
        return peopleService.findById(id);
    }
}
