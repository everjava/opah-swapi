package com.example.opahswapi.core.people.usecase;

import com.example.opahswapi.core.people.entity.People;

public interface PeopleFindById {
    People execute(Integer id);
}
