package com.example.opahswapi.core.people.usecase;


import com.example.opahswapi.core.people.entity.People;
import com.example.opahswapi.core.shared.entity.Results;
import com.example.opahswapi.core.people.ports.PeopleService;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PeopleFindAllUseCase implements  PeopleFindAll {

    private PeopleService peopleService;

    public Results<People> execute(){
            return  peopleService.findAll();
    }
}
