package com.example.opahswapi.core.people.ports;

import com.example.opahswapi.core.people.entity.People;
import com.example.opahswapi.core.shared.entity.Results;

public interface PeopleService {

    People findById(Integer id);

    Results<People> findAll();

}
