package com.example.opahswapi.core.films.exception;

public class FilmException extends RuntimeException {
    private static final long serialVersionUID = -744290412016287182L;

    public FilmException() {
    }

    public FilmException(String message) {
        super(message);
    }

    public FilmException(String message, Throwable cause) {
        super(message, cause);
    }

    public FilmException(Throwable cause) {
        super(cause);
    }

    public FilmException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
