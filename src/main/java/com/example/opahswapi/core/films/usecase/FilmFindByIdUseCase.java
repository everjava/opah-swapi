package com.example.opahswapi.core.films.usecase;

import com.example.opahswapi.core.films.entity.Film;
import com.example.opahswapi.core.films.exception.FilmNotFoundException;
import com.example.opahswapi.core.films.ports.FilmService;
import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
public class FilmFindByIdUseCase implements  FilmFindById{

    private FilmService filmService;

    public Film execute(Integer id){
        return Optional.ofNullable(filmService.findById(id)).orElseThrow(()-> new FilmNotFoundException("No results"));
    }
}
