package com.example.opahswapi.core.films.ports;

import com.example.opahswapi.core.films.entity.Film;
import com.example.opahswapi.core.shared.entity.Results;

public interface FilmService {

    Film findById(Integer id);

    Results<Film> findAll();
}
