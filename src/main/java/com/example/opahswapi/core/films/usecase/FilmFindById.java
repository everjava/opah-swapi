package com.example.opahswapi.core.films.usecase;

import com.example.opahswapi.core.films.entity.Film;

public interface FilmFindById {

    Film execute(Integer id);
}
