package com.example.opahswapi.core.films.usecase;

import com.example.opahswapi.core.films.entity.Film;
import com.example.opahswapi.core.films.exception.FilmException;
import com.example.opahswapi.core.films.ports.FilmService;
import com.example.opahswapi.core.shared.entity.Results;
import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor
public class FilmFindAllUseCase implements  FilmFindAll{

    private FilmService filmService;

    public Results<Film> execute(){
        return Optional.ofNullable(filmService.findAll()).orElseThrow(() -> new FilmException("No results"));
    }
}
