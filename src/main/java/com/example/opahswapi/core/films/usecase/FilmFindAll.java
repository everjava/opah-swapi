package com.example.opahswapi.core.films.usecase;

import com.example.opahswapi.core.films.entity.Film;
import com.example.opahswapi.core.shared.entity.Results;


public interface FilmFindAll {
    Results<Film> execute();
}
