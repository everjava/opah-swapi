package com.example.opahswapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class OpahSwapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpahSwapiApplication.class, args);
    }

}
